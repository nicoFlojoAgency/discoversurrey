<?php 
/*
Template Name: Full Width Template
Template Post Type: page
*/

 get_header(); ?>

<?php $header_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
<section style="background-image: url(<?php echo $header_image[0]; ?>);" id="section-header">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>

<section id="section-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); the_content(); endwhile; endif; ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>