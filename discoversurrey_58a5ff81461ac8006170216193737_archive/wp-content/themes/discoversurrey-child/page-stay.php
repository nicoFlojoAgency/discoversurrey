<?php 
/*
Template Name: Stay (All Accomodations)
Template Post Type: page
*/

 get_header(); ?>

<?php $header_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
<section style="background-image: url(<?php echo $header_image[0]; ?>);" id="section-header">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>

<section id="section-page">
		<div class="row">
			<div class="container">
			<div class="col-md-12">
			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();
			?>
			<h2><?php the_title(); ?></h2>
			<p><?php the_content(); ?></p>
			<?php // End of the loop.
			endwhile;
			?>
			</div>
			</div>
				<?php 
					$args = array('post_type' => 'accommodations');
					$query = new WP_Query($args);
					while($query -> have_posts()) : $query -> the_post();
				?>
				<div class="row">
					<div class="card-group accommodation-listing">
						<div class="card" style="background-image:url('<?php the_field('accomm_image'); ?>');">
							<!--<img src="<?php the_field('accomm_image'); ?>" class="card img-fluid">-->
						</div>
						<div class="card accomodation-listing-info">
							<h2><a href="<?php the_field('accomm_url'); ?>"><?php the_title(); ?></a></h2>
							<a href="<?php the_field('accomm_instagram'); ?>"><img src="<?php bloginfo( 'template_directory' ); ?>/images/icon-grey-insta.png"></a>
							<a href="<?php the_field('accomm_facebook'); ?>"><img src="<?php bloginfo( 'template_directory' ); ?>/images/icon-grey-fb.png"></a>
							<a href="tel:<?php the_field('accomm_twitter'); ?>"><img src="<?php bloginfo( 'template_directory' ); ?>/images/icon-grey-twitter.png"></a>
							<h4><?php the_field('accomm_address'); ?></h4>
							<p><?php the_field('accomm_description'); ?></p>
							<a href="https://www.google.ca/maps/<?php the_field('accomm_address'); ?>" target="_blank"><img src="<?php bloginfo( 'template_directory' ); ?>/images/icon-Web.png"></a>
							<a href="<?php the_field('accomm_url'); ?>"><img src="<?php bloginfo( 'template_directory' ); ?>/images/icon-Map.png"></a>
							<a href="<?php the_field('accomm_email'); ?>"><img src="<?php bloginfo( 'template_directory' ); ?>/images/icon-Mail.png"></a>
							<a href="tel:<?php the_field('accomm_phone'); ?>"><img src="<?php bloginfo( 'template_directory' ); ?>/images/icon-Phone.png"></a>
							<a href="<?php the_field('accomm_trip_advisor'); ?>"><img src="<?php bloginfo( 'template_directory' ); ?>/images/icon-TripAdvisor.png"></a>
						</div>
					</div>
				</div>
				<?php endwhile; ?>


		</div>
	</div>
</section>

<?php get_footer(); ?>