<?php

// Set path to theme specific functions
$functions_path = TEMPLATEPATH . '/functions/';

// Theme specific functionality
require_once ( $functions_path . 'utilities.php');

?>
