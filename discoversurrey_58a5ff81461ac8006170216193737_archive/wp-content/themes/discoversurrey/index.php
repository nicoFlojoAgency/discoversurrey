<?php get_header(); ?>

<section id="section-blog">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<p class="date-author"><img src="<?php bloginfo( 'template_directory' ); ?>/images/green-flower.png"> <?php the_date(); ?><span>Author : <strong><?php the_author_posts_link(); ?></strong></span></p>
				<div class="featured-image"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?></a></div>
				<p><?php the_excerpt(); ?></p>
				<p><a href="<?php the_permalink(); ?>">Read more &raquo;</a></p>
				<?php endwhile; endif; ?>
				<p class="pagination"><?php echo paginate_links(); ?></p>
			</div>
			<div class="col-md-3">
				<?php // if ( !function_exists( "dynamic_sidebar" ) || !dynamic_sidebar( "Blog - Newsletter" ) ) : endif; ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>      