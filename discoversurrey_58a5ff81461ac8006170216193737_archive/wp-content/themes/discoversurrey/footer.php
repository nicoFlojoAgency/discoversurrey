
<footer>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<?php if ( dynamic_sidebar ( 'footer-subscribe' ) ); ?>
			</div>
			<div class="col-xs-12 col-md-2">
				<?php if ( dynamic_sidebar ( 'footer-magazine' ) ); ?>
			</div>
			<div class="col-xs-12 col-md-4">
				<?php if ( dynamic_sidebar ( 'footer-social' ) ); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-2">
				<?php if ( dynamic_sidebar ( 'footer-corporate' ) ); ?>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-2">
				<?php if ( dynamic_sidebar ( 'footer-explore' ) ); ?>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-2">
				<?php if ( dynamic_sidebar ( 'footer-stay' ) ); ?>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-md-push-2">
				<?php if ( dynamic_sidebar ( 'footer-contact' ) ); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-4">
				<p>&copy; <?php echo date( 'Y' ); ?> Discover Surrey. All rights reserved.</p>
			</div>
			<div class="col-xs-12 col-md-8">
				<ul class="footer-navigation">
					<li><a href="">About</a></li>
					<li><a href="">Careers</a></li>
					<li><a href="">Privacy Policy</a></li>
					<li><a href="">Contact</a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>
</body>
</html>