<?php get_header(); ?>

<section id="section-blog">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php if ( !function_exists( "dynamic_sidebar" ) || !dynamic_sidebar( "Blog - Top Ad" ) ) : endif; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-9">
				<?php query_posts( 'posts_per_page=2' ); $i=0; if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<p class="date-author"><img src="<?php bloginfo( 'template_directory' ); ?>/images/green-flower.png"> <?php the_date(); ?><span>Author : <strong><?php the_author_posts_link(); ?></strong></span></p>
				<div class="featured-image"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?></a></div>
				<p><?php the_excerpt(); ?></p>
				<p><a href="<?php the_permalink(); ?>">Read more &gt;</a></p>
				<p class="comments-social"><?php comments_number( 'No comments', '1 comment', '% comments' ); ?><span><a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" title="Share on Facebook."><i class="fa fa-facebook"></i></a>
 <a href="https://twitter.com/home/?status=<?php the_title(); ?> - <?php the_permalink(); ?>" title="Tweet this!"><i class="fa fa-twitter"></i></a> <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); echo $url; ?>"><i class="fa fa-pinterest"></i></a>
</span></p>
				<?php $i++; if ( $i < 2 ) : ?>
				<hr>
				<?php endif; endwhile; endif; ?>
				<p class="pagination"><?php echo paginate_links(); ?></p>
			</div>
			<div class="col-md-3">
				<?php if ( !function_exists( "dynamic_sidebar" ) || !dynamic_sidebar( "Blog - Right Top Ad" ) ) : endif; ?>
				<?php if ( !function_exists( "dynamic_sidebar" ) || !dynamic_sidebar( "Blog - Facebook" ) ) : endif; ?>
				<?php if ( !function_exists( "dynamic_sidebar" ) || !dynamic_sidebar( "Blog - Right Bottom Ad" ) ) : endif; ?>
				<?php if ( !function_exists( "dynamic_sidebar" ) || !dynamic_sidebar( "Blog - Top Posts" ) ) : endif; ?>
				<?php if ( !function_exists( "dynamic_sidebar" ) || !dynamic_sidebar( "Blog - Search" ) ) : endif; ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>      