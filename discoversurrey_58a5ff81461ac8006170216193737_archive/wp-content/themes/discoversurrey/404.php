<?php get_header(); ?>

<section id="section-intro">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p>The page you're looking for either doesn't exist or something went wrong.</p>
				<p>Please go back to the home page by clicking on our logo and start again from there.</p>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>