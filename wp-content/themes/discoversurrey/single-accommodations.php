<?php get_header(); ?>

<?php $header_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
<section style="background-image: url(<?php echo $header_image[0]; ?>);" id="section-header">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>

<section id="section-blog">
	<div class="container">
		<div class="row">
			<div class="col-md-9">

			HI THIS IS A TEMPLATE
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<p class="date-author"><img src="<?php bloginfo( 'template_directory' ); ?>/images/green-flower.png"> <?php the_date(); ?><span>Author : <strong><?php the_author_posts_link(); ?></strong></span></p>
				<div class="featured-image"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?></a></div>
				<p><?php the_excerpt(); ?></p>

				<?php endwhile; endif; ?>
				
			</div>
			<div class="col-md-3">
				<?php // if ( !function_exists( "dynamic_sidebar" ) || !dynamic_sidebar( "Blog - Newsletter" ) ) : endif; ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?> 